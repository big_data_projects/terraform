variable "host_os" {
  type    = string
  default = "linux"
}

variable "getting_started_eks_node_role" {
  description  = "IAM node policiies to be attached to role"
  type = list(string)
}