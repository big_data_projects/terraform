#https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role


############################################
#### Allow EC2 to have permissions to S3
############################################
resource "aws_iam_role" "ec2_role_new" {
  name = "ec2_role_new"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "dev_node"
  }
}


resource "aws_iam_role_policy" "custom_ec2_policy_role" {
  name = "custom_ec2_policy_role"
  role = aws_iam_role.ec2_role_new.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetBucketLocation",
          "s3:ListAllMyBuckets",
          "s3:ListBucket"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::aws-de-udemy"
      },
      {
        Action = [
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::aws-de-udemy/*"
      },
      {
        Action = [
          "s3:GetObject",
          "s3:PutObject"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::awsglue-datasets/examples/*"
      }
    ]
  })
}

resource "aws_iam_instance_profile" "ec2_profile" {
  name = "ec2_profile"
  role = aws_iam_role.ec2_role_new.name
}

resource "aws_iam_role_policy_attachment" "eks-role-policy-attach-for-ec2" {
  role       = aws_iam_role.ec2_role_new.name
  policy_arn = data.aws_iam_policy.AmazonEKSClusterPolicy.arn
}

resource "aws_iam_role_policy_attachment" "eks-node-role-policy-attachment-for-ec2" {
  role       = "${aws_iam_role.ec2_role_new.name}"
  count      = "${length(var.getting_started_eks_node_role)}"
  policy_arn = "${var.getting_started_eks_node_role[count.index]}"
}
########
### EKS
#######
# https://www.youtube.com/watch?v=QThadS3Soig&list=PLHq1uqvAteVsUhzNBkn-rPzXtPNpJu1-k&index=4


# Control plane

resource "aws_iam_role" "eks_cluster_role" {
  name = "eks_cluster_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "eks.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "dev_node"
  }
}

resource "aws_iam_role_policy_attachment" "eks-role-policy-attach" {
  role       = aws_iam_role.eks_cluster_role.name
  policy_arn = data.aws_iam_policy.AmazonEKSClusterPolicy.arn
}


resource "aws_iam_instance_profile" "eks_cluster_profile" {
  name      = "eks_cluster_profile"
  role      = aws_iam_role.eks_cluster_role.name
}

# Nodes

resource "aws_iam_role" "getting_started_eks_node_role" {
  name = "getting_started_eks_node_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })

  tags = {
    tag-key = "dev_node"
  }
}

resource "aws_iam_role_policy_attachment" "eks-node-role-policy-attachment" {
  role       = "${aws_iam_role.getting_started_eks_node_role.name}"
  count      = "${length(var.getting_started_eks_node_role)}"
  policy_arn = "${var.getting_started_eks_node_role[count.index]}"
}

# resource "aws_iam_role_policy_attachment" "eks-node-role-policy-attachment" {
#   for_each = toset([
#     "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
#     "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
#     "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
#   ])

#   role       = var.iam_role_name
#   policy_arn = each.value
# }
