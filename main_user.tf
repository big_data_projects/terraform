# Create user
resource "aws_iam_user" "glue_user" {
  name = "glue_user"

  tags = {
    tag-key = "glue"
  }
}
# Create a user group
resource "aws_iam_group" "developers" {
  name = "developers"
}

# Assigment
resource "aws_iam_group_membership" "assigment" {
  name = "assigment"

  users = [
    aws_iam_user.glue_user.name
  ]

  group = aws_iam_group.developers.name
}

resource "aws_iam_policy_attachment" "glue-role-policy-attach-to-group" {
  name       = "attachment_group"
  groups     = [aws_iam_group.developers.name]
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"

}
# Manged policy 
resource "aws_iam_policy" "custom-s3-bucket-glue-managed-policy" {
  name = "manged_policy_for_glue"
  

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetBucketLocation",
          "s3:ListAllMyBuckets"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::aws-de-udemy"
      },
      {
        Action = [
          "s3:PutObject",
          "s3:GetObject",
          "s3:DeleteObject"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::aws-de-udemy/*"
      },
      {
        Action = [
          "s3:GetObject",
          "s3:PutObject"
        ]
        Effect   = "Allow"
        Resource = "arn:aws:s3:::awsglue-datasets/examples/*"
      }
    ]
  })
}