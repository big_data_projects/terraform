resource "aws_vpc" "mtc_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "dev"
  }
}

resource "aws_subnet" "mtc_public_subnet" {
  vpc_id                  = aws_vpc.mtc_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "dev-public"
  }
}
resource "aws_internet_gateway" "mtc_internet_gateway" {
  vpc_id = aws_vpc.mtc_vpc.id

  tags = {
    Name = "dev-igw"
  }
}

resource "aws_route_table" "mtc_public_rt" {
  vpc_id = aws_vpc.mtc_vpc.id

  tags = {
    Name = "dev-rt"
  }
}

resource "aws_route" "deafult_route" {
  route_table_id         = aws_route_table.mtc_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.mtc_internet_gateway.id
}

resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.mtc_public_subnet.id
  route_table_id = aws_route_table.mtc_public_rt.id
}

resource "aws_security_group" "mtc_sg" {
  name        = "dev_sg"
  description = "Dev security group"
  vpc_id      = aws_vpc.mtc_vpc.id

  ingress {
    description = "TLS from VPC"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["193.238.66.166/32"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}


resource "aws_key_pair" "mtc_auth" {
  key_name   = "mtckey"
  public_key = file("~/.ssh/terraform.pub")

}

resource "aws_instance" "dev_node" {
  instance_type = "t2.medium"
  ami           = data.aws_ami.server_ami.id
  # Adding the public key
  key_name               = aws_key_pair.mtc_auth.id
  vpc_security_group_ids = [aws_security_group.mtc_sg.id]
  subnet_id              = aws_subnet.mtc_public_subnet.id
  # Run the bash script. Installing the docker and EKCCLI
  user_data = file("userdata.tpl")
  # Adding the role to EC2
  iam_instance_profile = aws_iam_instance_profile.ec2_profile.name
  root_block_device {
    volume_size = 10
  }

  tags = {
    Name = "dev_node"
  }
  # Adding the public ip,user and identityfile ~/.ssh/config 
  provisioner "local-exec" {
    command = templatefile("${path.module}/${var.host_os}-ssh-config.tpl", {
      hostname     = self.public_ip,
      user         = "ubuntu",
      identityfile = "~/.ssh/terraform"
    })
    interpreter = var.host_os == "linux" ? ["bash", "-c"] : ["Powershell", "-Command"]
  }
}

##########
## S3
#########

# resource "aws_s3_bucket" "b" {
#   bucket = "aws-de-udemy"

#   tags = {
#     Name = "aws_de_udemy"
#   }
# }

# resource "aws_s3_object" "glue_s3_result" {
#   key    = "glue_result/"
#   bucket = aws_s3_bucket.b.id
# }

# resource "aws_s3_object" "glue_s3_script" {
#   key    = "script/"
#   bucket = aws_s3_bucket.b.id
# }


########
### Glue data catalog
########

# resource "aws_glue_catalog_database" "aws_glue_database" {
#   name = "medicare"
# }

# resource "aws_glue_crawler" "example_medicare" {
#   database_name = aws_glue_catalog_database.aws_glue_database.name
#   name          = "crawler_medicare"
#   role          = aws_iam_role.glue_access_role.arn

#   s3_target {
#     path = "s3://awsglue-datasets/examples/medicare/"
#   }
# }

