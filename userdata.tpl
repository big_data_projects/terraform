#!bin/bash
sudo apt-get update -y &&
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    awscli \
    git &&
curl -fsSL https://download.docker.com/linux/ubuntu/gpg |sudo apt-key add - &&
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" &&
sudo apt-get update -y &&
sudo apt-get install docker-ce docker-ce-cli containerd.io -y &&
sudo usermod -aG docker $USER

curl --fail --silent --show-error --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp &&
sudo mv /tmp/eksctl /usr/local/bin

git clone https://github.com/marclamberti/airflow-materials-aws.git

curl --fail --silent --show-error --location -O "https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl"
chmod +x ./kubectl
# Move the binary in to your PATH
sudo mv ./kubectl /usr/local/bin/kubectl

