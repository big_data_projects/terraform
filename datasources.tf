############
### EC2
############

data "aws_ami" "server_ami" {
  most_recent = true
  owners      = ["099720109477"]

  filter {
    name = "name"
    #   AMI name
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

########
### EKS
#######

data "aws_iam_policy" "AmazonEKSClusterPolicy" {
  arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

###############
## IAM Glue
###############

# data "aws_iam_policy_document" "glue-assume-role-policy" {
#   statement {
#     actions = ["sts:AssumeRole"]

#     principals {
#       type        = "Service"
#       identifiers = ["glue.amazonaws.com"]
#     }
#   }
# }
# COmmnet out due to missing manage Role AWSGlueServiceRole in cloud guru
# data "aws_iam_policy" "AWSGlueServiceRole-create-by-terraform" {
#   arn = "arn:aws:iam::aws:policy/service-role/AWSGlueServiceRole"
# }

# {
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "glue:*",
#                 "s3:GetBucketLocation",
#                 "s3:ListBucket",
#                 "s3:ListAllMyBuckets",
#                 "s3:GetBucketAcl",
#                 "ec2:DescribeVpcEndpoints",
#                 "ec2:DescribeRouteTables",
#                 "ec2:CreateNetworkInterface",
#                 "ec2:DeleteNetworkInterface",				
#                 "ec2:DescribeNetworkInterfaces",
#                 "ec2:DescribeSecurityGroups",
#                 "ec2:DescribeSubnets",
#                 "ec2:DescribeVpcAttribute",
#                 "iam:ListRolePolicies",
#                 "iam:GetRole",
#                 "iam:GetRolePolicy",
#                 "cloudwatch:PutMetricData"                
#             ],
#             "Resource": [
#                 "*"
#             ]
#         },
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:CreateBucket",
#                 "s3:PutBucketPublicAccessBlock"
#             ],
#             "Resource": [
#                 "arn:aws-cn:s3:::aws-glue-*"
#             ]
#         },
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:GetObject",
#                 "s3:PutObject",
#                 "s3:DeleteObject"				
#             ],
#             "Resource": [
#                 "arn:aws-cn:s3:::aws-glue-*/*",
#                 "arn:aws-cn:s3:::*/*aws-glue-*/*"
#             ]
#         },
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:GetObject"
#             ],
#             "Resource": [
#                 "arn:aws-cn:s3:::crawler-public*",
#                 "arn:aws-cn:s3:::aws-glue-*"
#             ]
#         },
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "logs:CreateLogGroup",
#                 "logs:CreateLogStream",
#                 "logs:PutLogEvents",
#                 "logs:AssociateKmsKey"                
#             ],
#             "Resource": [
#                 "arn:aws-cn:logs:*:*:/aws-glue/*"
#             ]
#         },
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "ec2:CreateTags",
#                 "ec2:DeleteTags"
#             ],
#             "Condition": {
#                 "ForAllValues:StringEquals": {
#                     "aws:TagKeys": [
#                         "aws-glue-service-resource"
#                     ]
#                 }
#             },
#             "Resource": [
#                 "arn:aws-cn:ec2:*:*:network-interface/*",
#                 "arn:aws-cn:ec2:*:*:security-group/*",
#                 "arn:aws-cn:ec2:*:*:instance/*"
#             ]
#         }
#     ]
# }

# data "aws_iam_policy_document" "aws_glue_service" {
#     statement {
#       effect = "Allow"
#       actions = [
#               "glue:*",
#               "s3:GetBucketLocation",
#               "s3:ListBucket",
#               "s3:ListAllMyBuckets",
#               "s3:GetBucketAcl",
#               "ec2:DescribeVpcEndpoints",
#               "ec2:DescribeRouteTables",
#               "ec2:CreateNetworkInterface",
#               "ec2:DeleteNetworkInterface",				
#               "ec2:DescribeNetworkInterfaces",
#               "ec2:DescribeSecurityGroups",
#               "ec2:DescribeSubnets",
#               "ec2:DescribeVpcAttribute",
#               "iam:ListRolePolicies",
#               "iam:GetRole",
#               "iam:GetRolePolicy",
#               "cloudwatch:PutMetricData"  
#         ]
#       resource = [
#           "*"
#       ]
#     }

#     statement {
#       effect = "Allow"
#       actions = [
#           "s3:CreateBucket",
#           "s3:PutBucketPublicAccessBlock"
#         ]
#       resource = [
#             "arn:aws-cn:s3:::aws-glue-*"
#         ]
#     }

#     statement {
#       effect = "Allow"
#       actions = [
#           "s3:GetObject",
#           "s3:PutObject",
#           "s3:DeleteObject"	
#         ]
#       resource = [
#           "arn:aws-cn:s3:::crawler-public*",
#           "arn:aws-cn:s3:::aws-glue-*"
#         ]
#     }

#     statement {
#         effect = "Allow"
#         actions = [
#             "s3:GetObject"
#         ]
#         resource = [
#             "arn:aws-cn:s3:::crawler-public*",
#             "arn:aws-cn:s3:::aws-glue-*"
#         ]
#     }

#     statement {
#       effect = "Allow"
#       actions = [
#         "s3:GetObject",
#         "s3:PutObject"
#         ]
#       resource = [
#         "${aws_s3_bucket.example.arn}/*"
#         ]
#     }
#     statement {
#       effect = "Allow"
#       actions = [
#         "s3:GetObject",
#         "s3:PutObject"
#         ]
#       resource = [
#         "${aws_s3_bucket.example.arn}/*"
#         ]
#     }
#}