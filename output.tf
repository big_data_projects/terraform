output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.dev_node.id

}

output "public_ip" {
  description = "The public ip address"
  value       = aws_instance.dev_node.public_ip

}