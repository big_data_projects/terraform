host_os = "linux"
# https://docs.aws.amazon.com/eks/latest/userguide/create-node-role.html
getting_started_eks_node_role = [
    "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy",
    "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly",
    "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"]